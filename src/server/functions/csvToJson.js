const csvtojson = require('csvtojson');
const fs = require('fs');

/**
 * converted matches.csv file to .json file
 * @type {function}
 */
 csvtojson()
 .fromFile("src/data/matches.csv")
 .then((json) => {
   fs.writeFileSync("src/data/matchesData.json", JSON.stringify(json),"utf-8",(err) => {
     if(err) console.log(err);
   })
 });

/**
* converted deliveries.csv file to .json file
* @type {function}
*/
csvtojson()
 .fromFile("src/data/deliveries.csv")
 .then((json) => {
   fs.writeFileSync("src/data/deliveriesData.json", JSON.stringify(json),"utf-8",(err) => {
     if(err) console.log(err);
   })
 });