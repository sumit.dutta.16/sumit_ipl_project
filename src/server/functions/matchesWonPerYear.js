/**
 * Takes matches arrayOfObject and create json file for matches won per team per year
 * @param {Array<{object}>} matches
 * @return {Array<{objects}>}
 */
module.exports = function(matches){
    
/**
 * contains all teams and maps to number of matches won per year
 * @type {{string : {string:Number}}}
 */
    const winCountOfTeams = matches.reduce((accumulator, match) => {

        let team = match.winner;
        let year = match.season;

        if(team === ""){
            return accumulator;
        }

        if(accumulator[team] === undefined){
            accumulator[team] = {};
            accumulator[team][year] = 1;
        }else if(accumulator[team][year] === undefined){
            accumulator[team][year] = 1;
        }else{
            accumulator[team][year] += 1;
        }
        return accumulator;
    }, {});

/**
 * contains all years and number of matches played in that year
 * @type {Array<{team:string , matches_won_per_year:{string:Number}}>}
 */
    const perYearMatchesWon = [];
    Object.keys(winCountOfTeams)
        .forEach(key => perYearMatchesWon.push({
            team : key,
            matches_won_per_year : winCountOfTeams[key]
        }));

    return perYearMatchesWon;
}